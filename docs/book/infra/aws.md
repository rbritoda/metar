# Amazon AWS

Amazon Web Services (AWS) offers a managed Kubernetes service called [EKS](https://aws.amazon.com/eks/).

It relies heavily on Cloud Formation and even if a higher level interface is
used you can still see the CF stacks being created underneath.

## Resources

The available instance types [are listed
here](https://aws.amazon.com/ec2/instance-types/).

We're particularly interested in the GPU types listed under [Accelerated Computing](https://aws.amazon.com/ec2/instance-types). As of the last update of this document we're relying on:
* p3: Nvidia V100
* p4d.24xlarge: preview and only with 8 GPUs per node for now (not used yet)

## Credential Setup
https://aws.amazon.com/premiumsupport/knowledge-center/iam-assume-role-cli/
https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-role.html

```bash
aws configure
...
```

```bash
cat ~/.aws/config
[default]
region = eu-central-1

[profile admin]
role_arn = arn:aws:iam::124939657953:role/AccountAdministrator
source_profile = default

[profile poweruser]
role_arn = arn:aws:iam::124939657953:role/AccountPowerUser
source_profile = default
```

```bash
cat ~/.aws/credentials
[default]
aws_access_key_id = ...
aws_secret_access_key = ...

[profile admin]
role_arn = arn:aws:iam::124939657953:role/AccountAdministrator
source_profile = default

[profile poweruser]
role_arn = arn:aws:iam::124939657953:role/AccountPowerUser
source_profile = default
```
```bash
eksctl --profile admin --region eu-central-1 get cluster
```

## Cluster Setup

## Using Config
```bash
apiVersion: eksctl.io/v1alpha5
kind: ClusterConfig

metadata:
  name: aws-eu-central-1-003
  region: eu-central-1

managedNodeGroups:
  - name: main
    instanceType: m5.large
    desiredCapacity: 1
    volumeSize: 80
  - name: arm
    instanceTypes: ["m6g.large", "m6gd.large"]
    spot: true
    labels:
      arm: "true"
    tags:
      k8s.io/cluster-autoscaler/node-template/label/arm: "true"
```

## Adding NodeGroups

Update the config file with the new nodegroups (nodegroups are then immutable,
can only be `eksctl scale ...`:
```bash
nodeGroups:
  - name: armbare
    instanceType: m6g.metal
    desiredCapacity: 0
    maxSize: 1
    minSize: 0
    volumeSize: 80
    labels:
      armbare: "true"
    tags:
      k8s.io/cluster-autoscaler/node-template/label/armbare: "true"
```
```bash
eksctl -p admin create nodegroup -f aws-cluster.yaml
```

## Manual
```bash
eksctl create cluster --name eks-eu-central-1 --region eu-central-1 --nodes 1 --profile poweruser
```

```bash
eksctl create nodegroup --cluster eks-eu-central-1 --region eu-central-1 \
    --name v100 --node-type p3.2xlarge --nodes 1 --node-labels gpu=v100 --profile poweruser
```

## Credentials

```
eksctl utils write-kubeconfig --cluster eks-gpubench-100 --profile poweruser
[ℹ]  eksctl version 0.31.0
[ℹ]  using region us-east-2
[✔]  saved kubeconfig as "/home/ricardo/.kube/config"


kubectl get node -o wide
```


## GPU Setup

## Billing

https://console.aws.amazon.com/billing/home?region=us-east-2#/
