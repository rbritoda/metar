# Overview

## Main Cluster
```bash

```

## ArgoCD Setup

```bash
kubectl create namespace argocd
helm repo add argo https://argoproj.github.io/argo-helm
helm repo update
helm -n argocd install argocd argo/argo-cd --values argocd/values.yaml
```

## ArgoCD Dashboard

```bash
kubectl -n argocd get service argocd-server
NAME            TYPE           CLUSTER-IP      EXTERNAL-IP      PORT(S)                      AGE
argocd-server   LoadBalancer   10.254.185.69   137.138.226.58   80:31521/TCP,443:31429/TCP   6m46s
```
```bash
curl https://137.138.226.58
( admin : <default-password-is-the-argocd-server-pod-name> )
```

## ArgoCD cli
```bash
argocd login 138.138.226.58
WARNING: server certificate had error: x509: certificate signed by unknown authority. Proceed insecurely (y/n)? y
Username: admin
Password:
'admin' logged in successfully
Context '137.138.226.58' updated
```

NOTE: argocd doesn't seem to support http proxies, a local tunnel is required
outside cern
```
ssh -L 1111:137.138.226.58:443 lxplus.cern.ch
argocd login localhost:1111
```

## Register Clusters
```bash
kubectl config get-contexts
CURRENT   NAME                                                  CLUSTER                                               AUTHINFO                                              NAMESPACE
*         gke_it-atlas-cern_europe-west1-b_gke-europe-west1-b   gke_it-atlas-cern_europe-west1-b_gke-europe-west1-b   gke_it-atlas-cern_europe-west1-b_gke-europe-west1-b
```
```bash
argocd cluster add gke_it-atlas-cern_europe-west1-b_gke-europe-west1-b
INFO[0000] ServiceAccount "argocd-manager" created in namespace "kube-system"
INFO[0001] ClusterRole "argocd-manager-role" created
INFO[0001] ClusterRoleBinding "argocd-manager-role-binding" created
Cluster 'https://34.76.94.8' added

argocd cluster list
```

## Deploy main app
```bash
kubectl create -f metar.yaml
```

## Debugging helm

Example for services:
```bash
helm template --values values.yaml services/
```
