# Google GCP

https://cloud.google.com/compute/docs/gpus/gpu-regions-zones
NOTE: Check [this link](https://cloud.google.com/compute/docs/gpus#gpus-list)
for region/gpu availability. T4s are the only not available in us-central1-c at
the time of this writing

## Cluster

Creating the cluster:
```bash
gcloud container clusters create --cluster-version 1.20.6-gke.1000 --machine-type n1-standard-4 --disk-type pd-standard --disk-size 100 --num-nodes 1 --region europe-west4 --enable-ip-alias --enable-tpu cern-metar-europe-west4
```

If the cluster is supposed to use TPUs, the following flags are required:
```bash
--enable-ip-alias --enable-tpu
```

For TPU availability in the different regions, [check here](https://cloud.google.com/tpu/docs/types-zones).

Setup additional pools for [each GPU type available](https://cloud.google.com/compute/docs/gpus) in GCP:
```bash
$ cat gcp-flavors
cern-metar-europe-west4|europe-west4|europe-west4-a,europe-west4-b|a100|a2-highgpu-1g
cern-metar-europe-west4|europe-west4|europe-west4-a,europe-west4-b,europe-west4-c|v100|n1-standard-4
cern-metar-europe-west4|europe-west4|europe-west4-a|p100|n1-standard-4
cern-metar-europe-west4|europe-west4|europe-west4-b,europe-west4-c|t4|n1-standard-4

for t in $(cat gcp-flavors); do OLDIFS=$IFS; for i in $t; do IFS="|"; set -- $i; gcloud container node-pools create $4 --cluster $1 --region $2 --node-locations $3 --accelerator=type=nvidia-tesla-$4,count=1 --machine-type $5 --preemptible --enable-autoscaling --min-nodes 0 --max-nodes 10 --num-nodes 0 --node-labels gpu=$4; IFS=$OLDIFS; done done
```

List and check it's all as expected:
```bash
gcloud container clusters list
gcloud container node-pools list --zone europe-west4-a --cluster gke-europe-west4-a
```

Install the Nvidia device plugin daemonset:
```bash
kubectl apply -f https://raw.githubusercontent.com/GoogleCloudPlatform/container-engine-accelerators/master/nvidia-driver-installer/cos/daemonset-nvidia-v450.yaml
```

## TPUs

In addition to the flags at cluster creation, given we rely on a custom webhook
in the case of gitlab runners we need to apply a change in the GKE TPU webhook:
```bash
kubectl --context gke_it-atlas-cern_europe-west4-a_gke-europe-west4-a-2 -n gitlab-runner get mutatingwebhookconfigurations 
NAME                                                      WEBHOOKS   AGE
cloud-tpus-webhook-config                                 2          13h
gitlab-resources-webhook.gitlab.com                       1          13h
```
```bash
kubectl --context gke_it-atlas-cern_europe-west4-a_gke-europe-west4-a-2 -n gitlab-runner edit mutatingwebhookconfigurations cloud-tpus-webhook-config
reinvocationPolicy: IfNeeded
```

The default Never would mean that if the tpu webhook occurs prior to our gitlab
resources webhook it would never run against the Pod: our hook is the one
adding the resources map for TPUs, while the TPU webhook will only run against
Pods that have those resources defined.

https://github.com/kubernetes/enhancements/pull/1049/files
https://kubernetes.io/docs/reference/access-authn-authz/extensible-admission-controllers/#reinvocation-policy

