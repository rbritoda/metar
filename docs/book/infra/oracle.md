# install cli

# configure kubectl access

https://docs.oracle.com/en-us/iaas/Content/ContEng/Tasks/contengdownloadkubeconfigfile.htm

```bash
oci ce cluster create-kubeconfig --cluster-id ocid1.cluster.oc1.eu-frankfurt-1.aaaaaaaa57pmdax53iegvi2h4hefhzwpqvdnehcalyrfxl2iac6zjsjjikoa --file config --region eu-frankfurt-1 --token-version 2.0.0  --kube-endpoint PUBLIC_ENDPOINT
```

Create a service account based access entry in kubeconfig:
```bash
kubectl -n kube-system create serviceaccount admin-sa
kubectl create clusterrolebinding admin-sa --clusterrole=cluster-admin --serviceaccount=kube-system:admin-sa
TOKENNAME=`kubectl -n kube-system get serviceaccount/admin-sa -o jsonpath='{.secrets[0].name}'`
TOKEN=`kubectl -n kube-system get secret $TOKENNAME -o jsonpath='{.data.token}'| base64 --decode`
kubectl config set-credentials admin-sa --token=$TOKEN
kubectl config set-context --current --user=admin-sa
kubectl get po -A
```
