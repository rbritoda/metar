# Microsoft Azure

## Autoscaler
https://docs.microsoft.com/en-us/azure/aks/cluster-autoscaler

##
+NOTE: Check [this link](https://azure.microsoft.com/en-us/global-infrastructure/services/?products=virtual-machines)
+for region/gpu availability. T4s and A100s are
[currently](https://azure.microsoft.com/en-us/blog/bringing-ai-supercomputing-to-customers/)
[experimental](https://docs.microsoft.com/en-us/azure/virtual-machines/nct4-v3-series)

Creating the cluster (with an initial pool with P4s):
```bash
az aks create -g rg-gitlab-ci -n aks-gitlab-ci-20211104 \
    --kubernetes-version 1.20.9 --enable-cluster-autoscaler \
    --location westeurope --zones 1,2,3 --tags 'project: gitlab-ci'
```

```
az aks get-credentials -g rg-gitlab-ci --name aks-gitlab-ci-20211104
Merged "aks-gitlab-ci-20211104" as current context in /home/ricardo/.kube/config
```

Setup additional pools for [each GPU type
available](https://docs.microsoft.com/en-us/azure/virtual-machines/sizes-gpu) in Azure:
```bash
$ cat azure-flavors
p4,61,Standard_ND6s
k80,35,Standard_NC6
m60,50,Standard_NV6
p100,60,Standard_NC6s_v2
v100,70,Standard_NC6s_v3
t4,75,Standard_NC4as_T4_v3 


for t in $(cat azure-flavors); do
  OLDIFS=$IFS
  for i in $t; do
    IFS=","
    set -- $i
    az aks nodepool add --cluster-name aks-gitlab-ci-20211104 -g rg-gitlab-ci --name $1 --enable-cluster-autoscaler --max-count 1 --min-count 0 --node-count 0 --labels gpu=$1 --node-vm-size $3
    IFS=$OLDIFS
  done
done
```

List and check it's all as expected:
```bash
az aks nodepool list --cluster-name aks-gpubench-001 -g rg-gpubenchmark-test -o table 

Name       OsType    KubernetesVersion    VmSize                Count    MaxPods    ProvisioningState    Mode
---------  --------  -------------------  --------------------  -------  ---------  -------------------  ------
agentpool  Linux     1.18.8               Standard_DS2_v2       1        110        Succeeded            System
k80        Linux     1.18.8               Standard_NC6          0        110        Succeeded            User
m60        Linux     1.18.8               Standard_NV6          0        110        Succeeded            User
p100       Linux     1.18.8               Standard_NC6s_v2      0        110        Succeeded            User
p4         Linux     1.18.8               Standard_ND6s         0        110        Succeeded            User
t4         Linux     1.18.8               Standard_NC4as_T4_v3  0        110        Failed               User
v100       Linux     1.18.8               Standard_NC6s_v3      0        110        Succeeded            User
```

Install the Nvidia Device Plugin daemonset:
```yaml 
kubectl create namespace gpu-resources

kubectl apply -f docs/book/infra/azure-nvidia-device-plugin-ds.yaml
```


