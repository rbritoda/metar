---
jupytext:
  text_representation:
    extension: .md
    format_name: myst
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Simpletrack

Simpletrack is a simplified version of the SixTrack software:
* [http://sixtrack.web.cern.ch/SixTrack/](http://sixtrack.web.cern.ch/SixTrack/)
* [https://github.com/rdemaria/simpletrack](https://github.com/rdemaria/simpletrack)

> SixTrack is a single particle 6D symplectic tracking code optimized for long
term tracking in high energy rings. It is mainly used for the LHC for dynamic
aperture studies, tune optimization, and collimation studies.

This simplified setup is useful to evaluate the performance of different computing resources.

## Workload Results

```{note}
Metrics are collected with Prometheus and queried / converted to Pandas for the analysis below
```
The results below are presented as *events per second*.

```{code-cell} ipython3
:tags: ["remove-input"]

import json
import numpy as np
import pandas as pd
import requests
from urllib.parse import urljoin

api_url = "http://localhost:1234"

#{'metric': {'cloud': 'google', 'gpu': 'v100'},
#   'values': [[1605179184, '0.823857307434082'],
def _to_pandas(data):
    labels = list(dict.items()) 
    pd.DataFrame({
        list(r['metrics']):
             pd.Series((np.float64(v[1]) for v in r['values']),
                    index=(pd.Timestamp(v[0], unit='s') for v in r['values']))
        for r in data['result']})
        
def _do_query(path, params):
    resp = requests.get(urljoin(api_url, path), params=params)
    if not (resp.status_code // 100 == 200 or resp.status_code in [400, 422, 503]):
        resp.raise_for_status()

    response = resp.json()
    if response['status'] != 'success':
        raise RuntimeError('{errorType}: {error}'.format_map(response))

    return response['data']

def metric_name(metric):
    """Convert metric labels to standard form."""
    name = metric.get('__name__', '')
    labels = ','.join(('{}={}'.format(k, json.dumps(v)) for k, v in metric.items() if k != '__name__'))
    a= '{0}{{{1}}}'.format(name, labels)
    print("%s :: %s" % (metric, a))

def query_range(query, start, end, step, timeout=None):
    params = {'query': query, 'start': start, 'end': end, 'step': step}
    params.update({'timeout': timeout} if timeout is not None else {})

    return _do_query('api/v1/query_range', params)

import matplotlib.pyplot as plt
import numpy as np
```

```{code-cell} ipython3
:tags: ["remove-input"]
data = query_range('sum by(cloud, gpu) (events{workload="simpletrack"})', '2020-11-12T10:47:39Z', '2020-11-12T12:19:10Z', '15s')
df = pd.DataFrame({
        (r['metric']['cloud'], r['metric']['gpu']):
             pd.Series((np.float64(v[1]) for v in r['values']),
                    index=(pd.Timestamp(v[0], unit='s') for v in r['values']))
        for r in data['result']})
df2 = df.mean().unstack(level=0)
```

```{margin} Cell Highlight
The highlighted cells indicate the card giving best performance for each cloud
service
```

```{code-cell} ipython3
:tags: ["remove-input"]
pd.set_eng_float_format(accuracy=1, use_eng_prefix=True)
df3 = df.mean().unstack(level=1)
df3.columns = df3.columns.str.upper()
df3[['M60', 'P4', 'T4', 'K80', 'P100', 'V100']].style.highlight_max(
    color='lightblue', axis=1).format("{:.2e}", na_rep="n/a")
```

```{code-cell} ipython3
:tags: ["remove-input"]
df2.index = df2.index.str.upper()
fig, ax = plt.subplots(figsize=(11, 5))

ax.set_ylabel('Aproximate fit time (seconds)')
ax.set_xlabel('GPU Cards')
_ = df2.plot.bar(rot=0, ax=ax)
```

### Amazon AWS

```{code-cell} ipython3
:tags: ["hide-input"]
fig, ax = plt.subplots(figsize=(11, 5))

ax.set_ylabel('Aproximate fit time (seconds)')
ax.set_xlabel('AWS GPU Cards')
_ = df2['google'].plot.bar(rot=0, ax=ax)
```

## Cloud Pricing

```{note}
Prices for GPU only, virtual machine not included
```

