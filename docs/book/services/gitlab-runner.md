# GitLab Runner

## Adding New Runner

When adding a new runner it first has to be registered.

The helm chart takes a key - *runnerRegistrationToken* - which you can find in
a GitLab project under Settings / CI/CD. The registration token is project
specific.

Once you deploy the runner the first time, make sure you drop the registration
token from the helm chart and replace it with a *runnerToken*, so that the same
runner is referenced when pods restart - otherwise it would register a new
runner each time.

You can find the gitlab runner token by checking in the pod (this info is not
made available in the GitLab UI):
```bash
$ kubectl --context gke_it-atlas-cern_europe-west4-a_gke-europe-west4-a-2 -n gitlab-runner exec -it gitlab-runner-tpu-v3-8-gitlab-runner-54d648b665-rp7bn bash

bash-5.0$ gitlab-runner list
( check for Token )
```

